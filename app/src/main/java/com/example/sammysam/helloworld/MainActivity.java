package com.example.sammysam.helloworld;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.Time;
import android.util.Log;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity {
    public ArrayAdapter<String> mForecastAdapter;

    public MainActivity()
    {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Hi!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        String[] forecasts = {"Today - Sun", "Tomorrow - Clouds",
                "Weds - Foggy", "Thurs - Sun",
                "Fri - Clouds", "Sat - Foggy"};
        ArrayList<String> forecasts_list = new ArrayList<String>(Arrays.asList(forecasts));

        mForecastAdapter = new ArrayAdapter<String>(getApplicationContext(),
                R.layout.list_item_forecast,
                R.id.list_item_forecast_textview,
                forecasts_list);
        ListView listView = (ListView) findViewById(R.id.listview_forecast);
        listView.setAdapter(mForecastAdapter);
    }

    public class FetchWeatherTask extends AsyncTask<String, Void, String[]> {
        private final String LogTag = FetchWeatherTask.class.getSimpleName();

        @Override
        protected String[] doInBackground(String... params)
        {
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;
            String forecastJsonStr = null;

            try {
                // Construct the URL for the OpenWeatherMap query
                // Possible parameters are avaiable at OWM's forecast API page, at
                // http://openweathermap.org/API#forecast
                //URL url = new URL("http://api.openweathermap.org/data/2.5/forecast/daily?q=11432&mode=json&units=metric&cnt=7");

                // Create the request to OpenWeatherMap, and open the connection
                final String FORECAST_BASE_URL =
                        "http://api.openweathermap.org/data/2.5/forecast/daily?";
                final String QUERY_PARAM = "zip";
                final String FORMAT_PARAM = "mode";
                final String UNITS_PARAM = "units";
                final String DAYS_PARAM = "cnt";
                final String APPID_PARAM = "APPID";

                String locationQuery = params[0] + ",us";
                String format = "json";
                String units = "imperial";
                String appid_key = "e0588301cad2d293c65a48226acf26d9";
                int numDays = 14;

                Uri builtUri = Uri.parse(FORECAST_BASE_URL).buildUpon()
                        .appendQueryParameter(QUERY_PARAM, locationQuery)
                        .appendQueryParameter(FORMAT_PARAM, format)
                        .appendQueryParameter(UNITS_PARAM, units)
                        .appendQueryParameter(DAYS_PARAM, Integer.toString(numDays))
                        .appendQueryParameter(APPID_PARAM, appid_key)
                        .build();

                URL url = new URL(builtUri.toString());

                Log.v(LogTag, "url = " + url);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                // Read the input stream into a String
                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                    // But it does make debugging a *lot* easier if you print out the completed
                    // buffer for debugging.
                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0) {
                    // Stream was empty.  No point in parsing.
                    return null;
                }
                forecastJsonStr = buffer.toString();
            } catch (IOException e) {
                Log.e(LogTag, "Error ", e);
                // If the code didn't successfully get the weather data, there's no point in attemping
                // to parse it.
                return null;
            } finally {
                if (urlConnection != null) {
                    //urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e(LogTag, "Error closing stream", e);
                    }
                }
            }
            Log.v("forecast data", "I got " + forecastJsonStr);
            try {
                return ParseWeatherData(forecastJsonStr, 14);
            }
            catch (JSONException e)
            {
                Log.e("forecast data", "json error: " + e);
            }

            return null;
        }

        private String[] ParseWeatherData(String buffer, int numDays) throws JSONException
        {
            String[] ids = TimeZone.getAvailableIDs(-5 * 60 * 60 * 1000);
            // if no ids were returned, something is wrong. get out.
            if (ids.length == 0)
                System.exit(0);
            // create a Eastern Time time zone
            SimpleTimeZone pdt = new SimpleTimeZone(-5 * 60 * 60 * 1000, ids[0]);
            // daylight savings
            pdt.setStartRule(Calendar.APRIL, 1, Calendar.SUNDAY, 2 * 60 * 60 * 1000);
            pdt.setEndRule(Calendar.OCTOBER, -1, Calendar.SUNDAY, 2 * 60 * 60 * 1000);

            Calendar calendar = new GregorianCalendar();
            Date trialTime = new Date();
            calendar.setTime(trialTime);
            Map<Integer, String> day_of_week = new HashMap<Integer, String>();
            day_of_week.put(1, "Sunday");
            day_of_week.put(2, "Monday");
            day_of_week.put(3, "Tuesday");
            day_of_week.put(4, "Wednesday");
            day_of_week.put(5, "Thursday");
            day_of_week.put(6, "Friday");
            day_of_week.put(7, "Saturday");


            String[] forecasts = new String[numDays];

            JSONObject forecastJson = new JSONObject(buffer);
            JSONArray weatherArray = forecastJson.getJSONArray("list");

            for(int d = 0; d < numDays; d++)
            {
                JSONObject todays_weather = weatherArray.getJSONObject(d);
                String tempString = day_of_week.get(((calendar.get(Calendar.DAY_OF_WEEK)+d-1)%7+1)) + ":  ";
                tempString += todays_weather.getJSONArray("weather").getJSONObject(0).getString("main");
                tempString += " - " + todays_weather.getJSONObject("temp").getInt("max");
                tempString += "°/" + todays_weather.getJSONObject("temp").getInt("min")+"°";
                tempString += "  \n" + todays_weather.getJSONArray("weather").getJSONObject(0).getString("description");

                forecasts[d] = tempString;
                Log.v("forecast data", "for day "+d+": " + forecasts[1]);
            }

            return forecasts;
        }

        @Override
        protected void onPostExecute(String[] forecasts) {
            if (forecasts != null){
                mForecastAdapter.clear();
                for (String dayForecast: forecasts){
                    mForecastAdapter.add(dayForecast);
                }
            }
            super.onPostExecute(forecasts);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
        Log.v("menu", "something clicked");

        switch (item.getItemId()) {
            case R.id.action_settings:
                Log.v("menu", "you clicked settings");
                return true;
            case R.id.menu_refresh:
                Log.v("menu", "you clicked menu refresh");
                FetchWeatherTask weatherTask = new FetchWeatherTask();
                weatherTask.execute("10026");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

//        return super.onOptionsItemSelected(item);
    }
}
